在Ubuntu中，LightDM是用于图形用户界面登录的显示管理器（Display Manager）。通常情况下，密码校验是在用户登录到图形界面时，即LightDM启动之后进行的。然而，如果你希望在系统初始化（init）阶段进行密码校验，通常会采用不同的方法，例如在系统引导过程中使用GRUB密码来限制对系统的访问。

以下是一个简要的示例，演示如何通过GRUB密码来限制系统的访问：

1. **设置GRUB密码**：

   首先，你需要设置GRUB引导菜单的密码。你可以使用`grub-mkpasswd-pbkdf2`命令生成一个加密的密码，然后将其添加到GRUB配置文件中。

   ```bash
   grub-mkpasswd-pbkdf2
   ```

   输入并确认密码后，会生成一个加密的密码字符串。将这个字符串复制下来。

2. **编辑GRUB配置文件**：

   使用文本编辑器打开GRUB的配置文件。通常，GRUB的配置文件位于`/etc/default/grub`。

   ```bash
   sudo nano /etc/default/grub
   ```

   在配置文件中，找到`GRUB_CMDLINE_LINUX_DEFAULT`行，并在引号内添加以下内容：

   ```
   GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
   ```

   修改后的配置可能如下所示，其中`<your_password_hash>`应该替换为上一步中生成的密码哈希：

   ```
   GRUB_CMDLINE_LINUX_DEFAULT="quiet splash" --unrestricted
   GRUB_CMDLINE_LINUX="GRUB_CMDLINE_LINUX_DEFAULT="<your_password_hash>"
   ```

3. **更新GRUB**：

   保存并关闭GRUB配置文件。然后，运行以下命令来更新GRUB配置：

   ```bash
   sudo update-grub
   ```

4. **重启系统**：

   重启系统以使新的GRUB配置生效。此时，系统将要求输入GRUB密码才能继续引导。

请注意，这种方法限制了对系统的物理访问。在GRUB密码之前，用户必须输入正确的密码才能引导到Ubuntu系统。这并不是常见的图形密码校验，而是系统引导过程的密码校验。如果你需要更高级的安全性或者其他定制化的需求，你可能需要进一步研究和定制系统的引导过程。
