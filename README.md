# 应用组文档中心

用于存储共享文档知识。

文档中心采用构建和发布方式: **Sphinx + Gitlab + Readthedocs** 

## 本地修改验证流程

```bash
$ sudo apt install python3-sphinx python3-sphinx-rtd-theme # 安装本地验证依赖

$ cd docs/ # 进入文档编辑中心

$ vi xxx.rst # 新增、删除或者修改要共享的文档。可参照 rst 编写文档。如果需要建立共享库一级链接则需要同步编辑 index.rst

$ make html # 更新生成  _build/html/ 目录，当有警告时，必须要消除，不然同步到 readthedocs 平台会构建失败

$ firefox _build/html/index.html # 本地验证网页是否有问题
```

## 本地验证通过后，可以直接 git push 到仓库

当本地验证通过后，就可以 **git add, git commit, git push** 到远程仓库，这时便会自动触发 **readthedocs** 编译，大约等待1分钟，编译通过后可以正常访问 [应用组文档中心](https://bjyyz-test2.readthedocs.io/en/latest/) 。



